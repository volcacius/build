ifndef ARM_TOOLCHAIN
ARM_TOOLCHAIN := linaro
endif
CTNG_CONFIG=ct-ng-config-linux

include toolchains/arm/$(ARM_TOOLCHAIN).mk

# Configure GCC compiler version
GCC_MAJOR := $(shell $(CXX) -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell $(CXX) -dumpversion | cut -f2 -d.)
GCC_TAG   := $(GCC_MAJOR)$(GCC_MINOR)

ifdef CONFIG_TARGET_ARM_CORTEX_A15
TARGET_FLAGS := "-march=armv7-a -mtune=cortex-a15 -mfpu=neon --sysroot=${PLATFORM_SYSROOT}"
endif

ifdef CONFIG_TARGET_ARM_CORTEX_A9
TARGET_FLAGS := "-march=armv7-a -mtune=cortex-a9 -mfpu=neon --sysroot=${PLATFORM_SYSROOT}"
endif


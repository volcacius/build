ifndef ARM_TOOLCHAIN
ARM_TOOLCHAIN := crystax5
endif

include toolchains/arm/$(ARM_TOOLCHAIN).mk

# Configure GCC compiler version
GCC_MAJOR := $(shell $(CXX) -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell $(CXX) -dumpversion | cut -f2 -d.)
GCC_TAG := "46"

TARGET_FLAGS := "--sysroot=${PLATFORM_SYSROOT}"

ifndef ARM_TOOLCHAIN
ARM_TOOLCHAIN := ctng
endif
CTNG_CONFIG=ct-ng-config-android
platform_setup: copy_sysroot_to_out

include toolchains/arm/$(ARM_TOOLCHAIN).mk

# Configure GCC compiler version
GCC_MAJOR := $(shell $(CXX) -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell $(CXX) -dumpversion | cut -f2 -d.)
GCC_TAG   := $(GCC_MAJOR)$(GCC_MINOR)

TARGET_FLAGS := "--sysroot=${PLATFORM_SYSROOT}"

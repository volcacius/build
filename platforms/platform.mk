
.PHONY: platform_setup platform_check platform_distclean

# Target platform
PLATFORM := $(shell echo $(CONFIG_TARGET_PLATFORM) | tr -d '"')

# Add platform-specific targets
include build/platforms/$(PLATFORM).mk

# NOTE: It is a recursively expanded variable VOLUNTARILY.
#       g++ can be absent, as of now (e.g. the cross-compiler should be build).
PLATFORM_TARGET = $(shell $(CXX) -dumpmachine)
ifdef CONFIG_TARGET_LINUX_NATIVE_I386
PLATFORM_TARGET = "i386-pc-linux-gnu"
endif

# Update defaults not configured by platform specific files
CC ?= $(subst ++,cc,$(CXX))
PLATFORM_PATH ?= $(PATH)

